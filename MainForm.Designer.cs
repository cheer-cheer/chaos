﻿namespace Chaos
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.tabPage1 = new System.Windows.Forms.TabPage();
      this.guidFormatLabel = new System.Windows.Forms.Label();
      this.guidFormatComboBox = new System.Windows.Forms.ComboBox();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.button2 = new System.Windows.Forms.Button();
      this.button1 = new System.Windows.Forms.Button();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.label3 = new System.Windows.Forms.Label();
      this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.button3 = new System.Windows.Forms.Button();
      this.textBox3 = new System.Windows.Forms.TextBox();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.charPoolTextBox = new System.Windows.Forms.TextBox();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.fixedStringLengthCheckBox = new System.Windows.Forms.CheckBox();
      this.minStringLengthNumericUpDown = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.maxStringLengthNumericUpDown = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.fillCharPoolButton = new System.Windows.Forms.Button();
      this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.fillDigits2CharPoolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.fillUpper2CharPoolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.fillLower2CharPoolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.tabControl1.SuspendLayout();
      this.tabPage1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
      this.tabPage2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
      this.groupBox4.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.minStringLengthNumericUpDown)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.maxStringLengthNumericUpDown)).BeginInit();
      this.contextMenuStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // tabControl1
      // 
      this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.tabControl1.Controls.Add(this.tabPage2);
      this.tabControl1.Controls.Add(this.tabPage1);
      this.tabControl1.Location = new System.Drawing.Point(12, 12);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(435, 478);
      this.tabControl1.TabIndex = 0;
      // 
      // tabPage1
      // 
      this.tabPage1.Controls.Add(this.button2);
      this.tabPage1.Controls.Add(this.label1);
      this.tabPage1.Controls.Add(this.guidFormatLabel);
      this.tabPage1.Controls.Add(this.guidFormatComboBox);
      this.tabPage1.Controls.Add(this.groupBox2);
      this.tabPage1.Controls.Add(this.numericUpDown1);
      this.tabPage1.Location = new System.Drawing.Point(4, 24);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage1.Size = new System.Drawing.Size(427, 450);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "GUID";
      this.tabPage1.UseVisualStyleBackColor = true;
      // 
      // guidFormatLabel
      // 
      this.guidFormatLabel.Location = new System.Drawing.Point(6, 10);
      this.guidFormatLabel.Name = "guidFormatLabel";
      this.guidFormatLabel.Size = new System.Drawing.Size(75, 23);
      this.guidFormatLabel.TabIndex = 9;
      this.guidFormatLabel.Text = "格式:";
      this.guidFormatLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // guidFormatComboBox
      // 
      this.guidFormatComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.guidFormatComboBox.DisplayMember = "Description";
      this.guidFormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.guidFormatComboBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.guidFormatComboBox.FormattingEnabled = true;
      this.guidFormatComboBox.Items.AddRange(new object[] {
            "32 位数字",
            "32 位数字，连字符分隔",
            "花括号包围，连字符分隔",
            "括号包围，连字符分隔"});
      this.guidFormatComboBox.Location = new System.Drawing.Point(87, 10);
      this.guidFormatComboBox.Name = "guidFormatComboBox";
      this.guidFormatComboBox.Size = new System.Drawing.Size(328, 23);
      this.guidFormatComboBox.TabIndex = 0;
      this.guidFormatComboBox.ValueMember = "FormatString";
      this.guidFormatComboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
      // 
      // groupBox2
      // 
      this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox2.BackColor = System.Drawing.SystemColors.Window;
      this.groupBox2.Controls.Add(this.button1);
      this.groupBox2.Controls.Add(this.textBox1);
      this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.groupBox2.Location = new System.Drawing.Point(6, 101);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(415, 343);
      this.groupBox2.TabIndex = 8;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "结果";
      // 
      // button2
      // 
      this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.button2.Location = new System.Drawing.Point(6, 68);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(415, 27);
      this.button2.TabIndex = 2;
      this.button2.Text = "生成";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new System.EventHandler(this.button2_Click);
      // 
      // button1
      // 
      this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.button1.Location = new System.Drawing.Point(319, 310);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(90, 27);
      this.button1.TabIndex = 1;
      this.button1.Text = "导出";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Visible = false;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // textBox1
      // 
      this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.textBox1.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox1.Location = new System.Drawing.Point(6, 22);
      this.textBox1.Multiline = true;
      this.textBox1.Name = "textBox1";
      this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.textBox1.Size = new System.Drawing.Size(403, 315);
      this.textBox1.TabIndex = 0;
      // 
      // numericUpDown1
      // 
      this.numericUpDown1.Location = new System.Drawing.Point(87, 39);
      this.numericUpDown1.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
      this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericUpDown1.Name = "numericUpDown1";
      this.numericUpDown1.Size = new System.Drawing.Size(120, 23);
      this.numericUpDown1.TabIndex = 6;
      this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.button3);
      this.tabPage2.Controls.Add(this.label3);
      this.tabPage2.Controls.Add(this.numericUpDown2);
      this.tabPage2.Controls.Add(this.groupBox4);
      this.tabPage2.Controls.Add(this.groupBox3);
      this.tabPage2.Controls.Add(this.groupBox1);
      this.tabPage2.Location = new System.Drawing.Point(4, 24);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(427, 450);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "随机字符串";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // label3
      // 
      this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.label3.Location = new System.Drawing.Point(298, 3);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(75, 23);
      this.label3.TabIndex = 9;
      this.label3.Text = "数量:";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // numericUpDown2
      // 
      this.numericUpDown2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.numericUpDown2.Location = new System.Drawing.Point(301, 29);
      this.numericUpDown2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericUpDown2.Name = "numericUpDown2";
      this.numericUpDown2.Size = new System.Drawing.Size(120, 23);
      this.numericUpDown2.TabIndex = 8;
      this.numericUpDown2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // groupBox4
      // 
      this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox4.Controls.Add(this.textBox3);
      this.groupBox4.Location = new System.Drawing.Point(6, 269);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(415, 177);
      this.groupBox4.TabIndex = 6;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "结果";
      // 
      // button3
      // 
      this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.button3.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.button3.Location = new System.Drawing.Point(6, 236);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(415, 27);
      this.button3.TabIndex = 1;
      this.button3.Text = "生成";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new System.EventHandler(this.button3_Click);
      // 
      // textBox3
      // 
      this.textBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.textBox3.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox3.Location = new System.Drawing.Point(6, 22);
      this.textBox3.Multiline = true;
      this.textBox3.Name = "textBox3";
      this.textBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.textBox3.Size = new System.Drawing.Size(403, 149);
      this.textBox3.TabIndex = 0;
      // 
      // groupBox3
      // 
      this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox3.BackColor = System.Drawing.SystemColors.Window;
      this.groupBox3.Controls.Add(this.fillCharPoolButton);
      this.groupBox3.Controls.Add(this.charPoolTextBox);
      this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.groupBox3.Location = new System.Drawing.Point(6, 112);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(415, 118);
      this.groupBox3.TabIndex = 5;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "字符池";
      // 
      // charPoolTextBox
      // 
      this.charPoolTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.charPoolTextBox.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.charPoolTextBox.Location = new System.Drawing.Point(6, 22);
      this.charPoolTextBox.Multiline = true;
      this.charPoolTextBox.Name = "charPoolTextBox";
      this.charPoolTextBox.Size = new System.Drawing.Size(307, 90);
      this.charPoolTextBox.TabIndex = 0;
      // 
      // groupBox1
      // 
      this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox1.BackColor = System.Drawing.SystemColors.Window;
      this.groupBox1.Controls.Add(this.fixedStringLengthCheckBox);
      this.groupBox1.Controls.Add(this.minStringLengthNumericUpDown);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.maxStringLengthNumericUpDown);
      this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.groupBox1.Location = new System.Drawing.Point(6, 6);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(286, 91);
      this.groupBox1.TabIndex = 4;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "字符串长度";
      // 
      // fixedStringLengthCheckBox
      // 
      this.fixedStringLengthCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.fixedStringLengthCheckBox.Location = new System.Drawing.Point(6, 51);
      this.fixedStringLengthCheckBox.Name = "fixedStringLengthCheckBox";
      this.fixedStringLengthCheckBox.Size = new System.Drawing.Size(100, 23);
      this.fixedStringLengthCheckBox.TabIndex = 4;
      this.fixedStringLengthCheckBox.Text = "固定长度";
      this.fixedStringLengthCheckBox.UseVisualStyleBackColor = true;
      this.fixedStringLengthCheckBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
      // 
      // minStringLengthNumericUpDown
      // 
      this.minStringLengthNumericUpDown.Location = new System.Drawing.Point(6, 22);
      this.minStringLengthNumericUpDown.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
      this.minStringLengthNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.minStringLengthNumericUpDown.Name = "minStringLengthNumericUpDown";
      this.minStringLengthNumericUpDown.Size = new System.Drawing.Size(100, 23);
      this.minStringLengthNumericUpDown.TabIndex = 0;
      this.minStringLengthNumericUpDown.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
      this.minStringLengthNumericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
      // 
      // label4
      // 
      this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.label4.Location = new System.Drawing.Point(112, 22);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(61, 23);
      this.label4.TabIndex = 3;
      this.label4.Text = "～";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // maxStringLengthNumericUpDown
      // 
      this.maxStringLengthNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.maxStringLengthNumericUpDown.Location = new System.Drawing.Point(179, 22);
      this.maxStringLengthNumericUpDown.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
      this.maxStringLengthNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.maxStringLengthNumericUpDown.Name = "maxStringLengthNumericUpDown";
      this.maxStringLengthNumericUpDown.Size = new System.Drawing.Size(100, 23);
      this.maxStringLengthNumericUpDown.TabIndex = 1;
      this.maxStringLengthNumericUpDown.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
      this.maxStringLengthNumericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown3_ValueChanged);
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(6, 39);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(75, 23);
      this.label1.TabIndex = 10;
      this.label1.Text = "数量:";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // fillCharPoolButton
      // 
      this.fillCharPoolButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.fillCharPoolButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.fillCharPoolButton.Location = new System.Drawing.Point(319, 22);
      this.fillCharPoolButton.Name = "fillCharPoolButton";
      this.fillCharPoolButton.Size = new System.Drawing.Size(90, 27);
      this.fillCharPoolButton.TabIndex = 3;
      this.fillCharPoolButton.Text = "填写...";
      this.fillCharPoolButton.UseVisualStyleBackColor = true;
      this.fillCharPoolButton.Click += new System.EventHandler(this.fillCharPoolButton_Click);
      // 
      // contextMenuStrip1
      // 
      this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fillDigits2CharPoolToolStripMenuItem,
            this.fillUpper2CharPoolToolStripMenuItem,
            this.fillLower2CharPoolToolStripMenuItem});
      this.contextMenuStrip1.Name = "contextMenuStrip1";
      this.contextMenuStrip1.Size = new System.Drawing.Size(142, 70);
      // 
      // fillDigits2CharPoolToolStripMenuItem
      // 
      this.fillDigits2CharPoolToolStripMenuItem.Name = "fillDigits2CharPoolToolStripMenuItem";
      this.fillDigits2CharPoolToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
      this.fillDigits2CharPoolToolStripMenuItem.Text = "数字(&D)";
      this.fillDigits2CharPoolToolStripMenuItem.Click += new System.EventHandler(this.fillDigits2CharPoolToolStripMenuItem_Click);
      // 
      // fillUpper2CharPoolToolStripMenuItem
      // 
      this.fillUpper2CharPoolToolStripMenuItem.Name = "fillUpper2CharPoolToolStripMenuItem";
      this.fillUpper2CharPoolToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
      this.fillUpper2CharPoolToolStripMenuItem.Text = "大写字母(&U)";
      this.fillUpper2CharPoolToolStripMenuItem.Click += new System.EventHandler(this.fillUpper2CharPoolToolStripMenuItem_Click);
      // 
      // fillLower2CharPoolToolStripMenuItem
      // 
      this.fillLower2CharPoolToolStripMenuItem.Name = "fillLower2CharPoolToolStripMenuItem";
      this.fillLower2CharPoolToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
      this.fillLower2CharPoolToolStripMenuItem.Text = "小写字母(&L)";
      this.fillLower2CharPoolToolStripMenuItem.Click += new System.EventHandler(this.fillLower2CharPoolToolStripMenuItem_Click);
      // 
      // Form1
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.BackColor = System.Drawing.SystemColors.Window;
      this.ClientSize = new System.Drawing.Size(459, 502);
      this.Controls.Add(this.tabControl1);
      this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
      this.MaximizeBox = false;
      this.Name = "Form1";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Chaos";
      this.tabControl1.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
      this.tabPage2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.minStringLengthNumericUpDown)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.maxStringLengthNumericUpDown)).EndInit();
      this.contextMenuStrip1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.NumericUpDown numericUpDown1;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.ComboBox guidFormatComboBox;
    private System.Windows.Forms.Label guidFormatLabel;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.TextBox charPoolTextBox;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.CheckBox fixedStringLengthCheckBox;
    private System.Windows.Forms.NumericUpDown minStringLengthNumericUpDown;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.NumericUpDown maxStringLengthNumericUpDown;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.TextBox textBox3;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown numericUpDown2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button fillCharPoolButton;
    private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    private System.Windows.Forms.ToolStripMenuItem fillDigits2CharPoolToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem fillUpper2CharPoolToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem fillLower2CharPoolToolStripMenuItem;
  }
}

