﻿using System.Collections.Generic;

namespace Chaos
{
    static class GuidFormats
  {
    static readonly IList<GuidFormat> formats;
    static readonly GuidFormat n;
    static readonly GuidFormat d;
    static readonly GuidFormat b;
    static readonly GuidFormat p;

    static GuidFormats()
    {
      n = new GuidFormat("n", "32 位数字");
      d = new GuidFormat("d", "32 位数字，连字符分隔");
      b = new GuidFormat("b", "32 位数字，连字符分隔，大括号包围");
      p = new GuidFormat("p", "32 位数字，连字符分隔，小括号包围");

      formats = new List<GuidFormat>(4)
      {
        N,
        D,
        B,
        P
      }.AsReadOnly();
    }


    public static IList<GuidFormat> GetAll() => formats;

    public static GuidFormat N
    {
      get
      {
        return n;
      }
    }
    public static GuidFormat D
    {
      get
      {
        return d;
      }
    }
    public static GuidFormat B
    {
      get
      {
        return b;
      }
    }
    public static GuidFormat P
    {
      get
      {
        return p;
      }
    }
  }
}
