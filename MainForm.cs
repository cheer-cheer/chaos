﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Chaos
{
    public partial class MainForm
        : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            guidFormatComboBox.DataSource = GuidFormats.GetAll();
        }
        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        private void button2_Click(object sender, EventArgs e)
        {
            var format = guidFormatComboBox.SelectedValue as string;
            var number = (int)numericUpDown1.Value;

            textBox1.Text = string.Empty;

            for (var i = 0; i < number; i++)
            {
                var guid = Guid.NewGuid().ToString(format);

                if (i > 0)
                {
                    textBox1.AppendText(Environment.NewLine);
                }
                textBox1.AppendText(guid);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var file = SelectFileToSaveGeneratedGuids();
            if (!string.IsNullOrEmpty(file))
            {
                SaveGeneratedGuidsTo(file);
            }
        }

        private void SaveGeneratedGuidsTo(string file)
        {
            FileStream stream = null;
            StreamWriter writer = null;
            try
            {
                stream = File.OpenWrite(file);
                writer = new StreamWriter(stream, Encoding.UTF8);
                writer.Write(textBox1.Text);
                writer.Flush();
            }
            finally
            {
                writer?.Close();
            }
        }

        private string SelectFileToSaveGeneratedGuids()
        {
            using (var dlg = new SaveFileDialog())
            {
                dlg.Filter = "文本文件(*.txt)|*.txt|所有文件(*.*)|*.*";
                dlg.Title = "导出 GUID";
                dlg.DefaultExt = "txt";

                if (dlg.ShowDialog(this) == DialogResult.OK)
                {
                    return dlg.FileName;
                }
                return null;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (fixedStringLengthCheckBox.Checked)
            {
                label4.Visible = false;
                maxStringLengthNumericUpDown.Visible = false;
                minStringLengthNumericUpDown.Maximum = 999;
            }
            else
            {
                label4.Visible = true;
                maxStringLengthNumericUpDown.Minimum = minStringLengthNumericUpDown.Value;
                maxStringLengthNumericUpDown.Visible = true;
            }
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            if (!fixedStringLengthCheckBox.Checked && minStringLengthNumericUpDown.Focused)
            {
                maxStringLengthNumericUpDown.Minimum = minStringLengthNumericUpDown.Value;
            }
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            if (!fixedStringLengthCheckBox.Checked && maxStringLengthNumericUpDown.Focused)
            {
                minStringLengthNumericUpDown.Maximum = maxStringLengthNumericUpDown.Value;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var fixedLength = fixedStringLengthCheckBox.Checked;

            int stringLength = 0;
            int maxStringLength = 0;
            int minStringLength = 0;
            if (fixedLength)
            {
                stringLength = (int)minStringLengthNumericUpDown.Value;
            }
            else
            {
                minStringLength = (int)minStringLengthNumericUpDown.Value;
                maxStringLength = (int)maxStringLengthNumericUpDown.Value;
            }

            var rnd = new Random();
            var charPool = charPoolTextBox.Text;
            var numberOfStrings = (int)numericUpDown2.Value;

            textBox3.Text = string.Empty;
            for (var i = 0; i < numberOfStrings; i++)
            {
                if (!fixedLength)
                {
                    stringLength = rnd.Next(minStringLength, maxStringLength + 1);
                }

                if (i > 0)
                {
                    textBox3.AppendText(Environment.NewLine);
                }

                for (var j = 0; j < stringLength; j++)
                {
                    textBox3.AppendText(charPool[rnd.Next(0, charPool.Length)] + "");
                }
            }
        }

        private void fillCharPoolButton_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Show(fillCharPoolButton, new Point(0, fillCharPoolButton.Height));
        }

        private void fillDigits2CharPoolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FillCharPoolTextBox("0123456789");
        }
        private void fillUpper2CharPoolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FillCharPoolTextBox("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        }

        private void fillLower2CharPoolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FillCharPoolTextBox("abcdefghijklmnopqrstuvwxyz");
        }
        private void FillCharPoolTextBox(string input)
        {
            var selStart = charPoolTextBox.SelectionStart;
            charPoolTextBox.Text = charPoolTextBox.Text.Insert(selStart, input);
            charPoolTextBox.SelectionStart = selStart + input.Length;
            charPoolTextBox.Focus();
        }
    }
}
