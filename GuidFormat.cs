﻿using System;
using System.Linq;
using System.Text;

namespace Chaos
{
    sealed class GuidFormat
  {
    public string FormatString
    {
      get;
      private set;
    }
    public string Description
    {
      get;
      private set;
    }

    public GuidFormat(string formatString, string description)
    {
      this.FormatString = formatString;
      this.Description = description;
    }
  }
}
